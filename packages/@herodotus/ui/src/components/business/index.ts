import HDictionarySelect from './HDictionarySelect.vue';
import HInstitutionSelect from './HInstitutionSelect.vue';
import HTableItemStatus from './HTableItemStatus.vue';
import HOrganizationSelect from './HOrganizationSelect.vue';
import HOrganizationTree from './HOrganizationTree.vue';
import HDepartmentSelect from './HDepartmentSelect.vue';
import HDepartmentTree from './HDepartmentTree.vue';

export {
    HDictionarySelect,
    HInstitutionSelect,
    HTableItemStatus,
    HOrganizationSelect,
    HDepartmentSelect,
    HOrganizationTree,
    HDepartmentTree,
};
