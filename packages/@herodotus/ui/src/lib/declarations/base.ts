export interface Entity {}

export type ConstantDictionary = {
    key: string;
    text: string;
    value: number;
};
