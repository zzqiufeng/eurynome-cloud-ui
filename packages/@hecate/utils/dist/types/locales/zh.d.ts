declare const zh: {
    $vuetify: {
        badge: string;
        close: string;
        dataIterator: {
            noResultsText: string;
            loadingText: string;
        };
        dataTable: {
            itemsPerPageText: string;
            ariaLabel: {
                sortDescending: string;
                sortAscending: string;
                sortNone: string;
                activateNone: string;
                activateDescending: string;
                activateAscending: string;
            };
            sortBy: string;
        };
        dataFooter: {
            itemsPerPageText: string;
            itemsPerPageAll: string;
            nextPage: string;
            prevPage: string;
            firstPage: string;
            lastPage: string;
            pageText: string;
        };
        datePicker: {
            itemsSelected: string;
            nextMonthAriaLabel: string;
            nextYearAriaLabel: string;
            prevMonthAriaLabel: string;
            prevYearAriaLabel: string;
        };
        noDataText: string;
        carousel: {
            prev: string;
            next: string;
            ariaLabel: {
                delimiter: string;
            };
        };
        calendar: {
            moreEvents: string;
        };
        fileInput: {
            counter: string;
            counterSize: string;
        };
        timePicker: {
            am: string;
            pm: string;
        };
        pagination: {
            ariaLabel: {
                wrapper: string;
                next: string;
                previous: string;
                page: string;
                currentPage: string;
            };
        };
    };
};
export default zh;
//# sourceMappingURL=zh.d.ts.map