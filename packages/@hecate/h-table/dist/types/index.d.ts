import _Vue from 'vue';
import HTable from './HTable.vue';
declare const HTablePluginObject: {
    install(Vue: typeof _Vue): void;
};
export default HTable;
export { HTablePluginObject, HTable };
//# sourceMappingURL=index.d.ts.map