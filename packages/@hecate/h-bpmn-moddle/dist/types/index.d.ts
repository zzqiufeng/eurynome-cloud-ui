import _Vue from 'vue';
import HBpmnModdle from './HBpmnModdle.vue';
declare const HBpmnModdlePluginObject: {
    install(Vue: typeof _Vue): void;
};
export default HBpmnModdle;
export { HBpmnModdlePluginObject, HBpmnModdle };
//# sourceMappingURL=index.d.ts.map