import { Vuetify, VeeValidate, HecatePlugins } from './plugins';

export { Vuetify, VeeValidate, HecatePlugins };
