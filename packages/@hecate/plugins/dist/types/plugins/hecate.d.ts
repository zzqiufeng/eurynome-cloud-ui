import _Vue from 'vue';
declare const HecatePlugins: {
    install(Vue: typeof _Vue): void;
};
export default HecatePlugins;
//# sourceMappingURL=hecate.d.ts.map