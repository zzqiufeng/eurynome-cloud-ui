import _Vue from 'vue';
import HButton from './HButton.vue';
declare const HButtonPluginObject: {
    install(Vue: typeof _Vue): void;
};
export default HButton;
export { HButtonPluginObject, HButton };
//# sourceMappingURL=index.d.ts.map